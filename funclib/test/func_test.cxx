#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "func_lib.h"

TEST_CASE("Squares are computed", "[square]"){
    FuncLib funcLib;
    REQUIRE( funcLib.square(1) == 1 );
    REQUIRE( funcLib.square(2) == 4 );
    REQUIRE( funcLib.square(9) == 81 );
}
