cmake_minimum_required(VERSION 2.9)

project(MultiProjectTest CXX)

# GLOBAL VALUES
set(CATCH_DIR ${CMAKE_SOURCE_DIR}/catch)
set(CMAKE_BINARY_DIR ${CMAKE_SOURCE_DIR}/build)
set(EXECUTABLE_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)
set(LIBRARY_OUTPUT_PATH ${CMAKE_SOURCE_DIR}/bin)

# GLOBAL CHECKS
set(CMAKE_DISABLE_IN_SOURCE_BUILD ON)
set(CMAKE_DISABLE_SOURCE_CHANGES  ON)

if ("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
      message(SEND_ERROR "In-source builds are not allowed.")
endif ()

set(CMAKE_COLOR_MAKEFILE   ON)

add_subdirectory(funclib) #static functions lib
add_subdirectory(exec) #executable to export

# Tests
enable_testing()
add_library(Catch INTERFACE)
target_include_directories(Catch INTERFACE ${CATCH_DIR})
