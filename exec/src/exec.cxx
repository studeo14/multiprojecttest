
#include "func_lib.h"
#include <iostream>

int main(int argc, char**argv){
    //vars
    int input(0);
    FuncLib funcLib;
    //prompt
    std::cout << "Enter in a number to square (-1 to stop): ";
    std::cin >> input;
    while(input >= 0){
        std::cout << "Result: " << funcLib.square(input) << std::endl;
        std::cout << "Enter in a number to square (-1 to stop): ";
        std::cin >> input;
    }
    return 0;
}
